import ui from "./ui.js"

// Etat et actions du monstre
var name, life, money, awake;

function get() {
  return "Name : "+name+", life : "+life+", money : "+money+", awake : "+awake;
}

function init (n, l, m){
  name = n;
  life = l;
  money = m;
  awake = true;
}

/*
* Etape 3
*/
function run (){
  if (awake){
    if (life >= 1){
      life --;
    }
  }
  ui.displayStatus();
  ui.init();
}

function fight (){
  if (awake){
    if (life >= 3){
      life -= 3;
    }
  }
  ui.displayStatus();
  ui.init();
}

function work() {
  if (awake){
    if (life >= 1){
      life --;
      money += 2;
    }
  }
  ui.displayStatus();
  ui.init();
}

function eat (){
  if (awake){
    if (money >= 3){
      money -= 2;
      life ++ ;
    }
  }
  ui.displayStatus();
  ui.init();
}

function sleep() {
  //AFFICHER ETAT ui.displayStatus
  awake = false;
  setTimeout(function(){
              life++;
              awake = true;
      }, 10000);
  ui.displayStatus();
  ui.init();
}

function kill(){
  life = 0;
  money = 0;
  awake = true;
  ui.displayStatus();
  ui.init();
}


function interval (){
  //Interval
  setInterval(function(){
    life --;
    //Random
    let indice = Math.random();
    switch (true){
      case (indice < 0.25):
        run();
        break;
      case (indice >= 0.25 && indice <0.5):
        fight();
        break;
      case (indice >= 0.5 && indice < 0.75):
        work();
        break;
      case (indice >= 0.75):
        eat();
        break;
      }
    }, 12000);
  }


export default {
  init : init,
  get : get,
  run: run,
  fight : fight,
  eat : eat,
  work : work,
  sleep : sleep,
  interval : interval,
  kill : kill
};
