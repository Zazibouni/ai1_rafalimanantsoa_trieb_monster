import actions from "./actions.js";
//déroulement de l'application, handlers

//Etape 2 question 3 : Compléter le module app pour qu'il utilise ces 2 fonctions pour afficher l'état du monstre dans l'interface après son initialisation.




function start() {
  actions.init("Luxia",10,15);
  $('#b6').on('click', function() {
    alert(actions.get());
  });

  $('#b2').on('click', function() {
    actions.run();
  });

  $('#b3').on('click', function() {
    actions.fight();
  });

  $('#b7').on('click', function() {
    actions.work();
  });

  $('#b4').on('click', function() {
    actions.sleep();
  });

  $('#b5').on('click', function() {
    actions.eat();
  });

  $('#b1').on('click', function() {
    actions.init("Luxia",10,15);
  });

  $('#k').on('click', function() {
    actions.kill();
  });

}

export default {
  start : start
};
