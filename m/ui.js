import actions from './actions.js';

/*
* Modue qui permet d'afficher un message dans la boite ActionBox
*/
let log = function(message){
  let contenu = "<p>" + message + "</p>";
    $('#actionbox').prepend(contenu);
}

function displayStatus(){
	$('status').text(/*le text du status*/);
}
/*
let displayStatus = function(){ // = show Satus dans Etape 4 question 3
  //Oui, j'ai pas compris la question


  //Changement couleur de la boite Et-4 q.3
  if (life < 5){
    $("#monster").toggleClass("monsterRouge");
  } else if (life >== 5 && life < 15){
    $("#monster").toggleClass("monster");
  } else if (life >== 15 && life < 30){
    $("#monster").toggleClass("monsterVert");
  } else {
    $("#monster").toggleClass("monsterViolet");
  }

  //changement de la taille de la boite
  if (money < 5){
    $("#monster").removeClass("monsterMoyenne");
    $("#monster").addClass("monsterPetite");
  } else if (money >== 5 && money < 10){
    $("#monster").removeClass("monsterPetite");
    $("#monster").removeClass("monsterGrande");
    $("#monster").addClass("monsterMoyenne");
  } else{
    $("#monster").removeClass("monsterMoyenne");
    $("#monster").addClass("monsterGrandes");
  }
}
*/

/*fonction pour la taille de la boite*/
let init = function(){
  //$("#").on('click');
  //Changement couleur de la boite Et-4 q.3
  if (actions.life < 5){
    $("#monster").toggleClass("monsterRouge");
  } else if (actions.life >= 5 && actions.life < 15){
    $("#monster").toggleClass("monster");
  } else if (actions.life >= 15 && actions.life < 30){
    $("#monster").toggleClass("monsterVert");
  } else {
    $("#monster").toggleClass("monsterViolet");
  }

  //changement de la taille de la boite
  if (actions.money < 5){
    $("#monster").removeClass("monsterMoyenne");
    $("#monster").addClass("monsterPetite");
  } else if (actions.money >= 5 && actions.money < 10){
    $("#monster").removeClass("monsterPetite");
    $("#monster").removeClass("monsterGrande");
    $("#monster").addClass("monsterMoyenne");
  } else{
    $("#monster").removeClass("monsterMoyenne");
    $("#monster").addClass("monsterGrandes");
  }
}

export default{
  init : init,
  displayStatus : displayStatus,
  log : log
}
